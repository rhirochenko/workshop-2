//! Exonum Auction API.

use exonum::{
    api::{self, ServiceApiBuilder, ServiceApiState},
    crypto::{CryptoHash, PublicKey},
};

use crate::{auction::*, schema::Schema};

/// Auction service API description.
#[derive(Debug)]
pub struct AuctionApi;

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct UserQuery {
    pub user_id: PublicKey,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct LotQuery {
    pub lot_id: u64,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct AuctionQuery {
    pub auction_id: u64,
}

impl AuctionApi {
    /// User profile.
    fn get_user(state: &ServiceApiState, query: UserQuery) -> api::Result<Option<User>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);
        Ok(schema.users().get(&query.user_id))
    }

    /// All users.
    fn get_users(state: &ServiceApiState, _query: ()) -> api::Result<Vec<User>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);
        let idx = schema.users();
        let users: Vec<User> = idx.values().collect();
        Ok(users)
    }

    /// Auction Lot.
    fn get_lot(state: &ServiceApiState, query: LotQuery) -> api::Result<Option<Lot>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);
        Ok(schema.lots().get(&query.lot_id.hash()))
    }

    /// All auction lots.
    fn get_lots(state: &ServiceApiState, _query: ()) -> api::Result<Vec<Lot>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);

        let idx = schema.lots();
        let lots = idx.iter().map(|(_k, v)| v).collect::<Vec<_>>();
        Ok(lots)
    }

    /// User lots list.
    fn get_user_lots(state: &ServiceApiState, query: UserQuery) -> api::Result<Option<Vec<Lot>>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);

        Ok(schema.users().get(&query.user_id).map(|user| {
            let user_lots = schema.user_lots(&user.user_id);
            user_lots
                .into_iter()
                .map(|lot_id| schema.lots().get(&lot_id.hash()).unwrap())
                .collect()
        }))
    }

    /// Auctions made by user.
    fn get_users_auctions(
        state: &ServiceApiState,
        query: UserQuery,
    ) -> api::Result<Option<Vec<Auction>>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);

        Ok(schema.users().get(&query.user_id).map(|user| {
            let user_auctions = schema.user_auctions(&user.user_id);
            user_auctions
                .into_iter()
                .map(|auction_id| schema.auctions().get(&auction_id.hash()).unwrap())
                .collect()
        }))
    }

    /// Auctions and bids by auction identifier.
    fn get_auction_with_bids(
        state: &ServiceApiState,
        query: AuctionQuery,
    ) -> api::Result<Option<(Auction, Vec<Bid>)>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);

        Ok(schema
            .auctions()
            .get(&query.auction_id.hash())
            .map(|auction| {
                let bids = schema.bids(auction.auction_id);
                let bids = bids.into_iter().collect();
                (auction, bids)
            }))
    }

    /// Auction bids by its identifier.
    fn get_auction_bids(
        state: &ServiceApiState,
        query: AuctionQuery,
    ) -> api::Result<Option<Vec<Bid>>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);

        Ok(schema
            .auctions()
            .get(&query.auction_id.hash())
            .map(|auction| {
                let bids = schema.bids(auction.auction_id);
                bids.into_iter().collect()
            }))
    }

    /// All auctions.
    fn get_auctions(state: &ServiceApiState, _query: ()) -> api::Result<Vec<Auction>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);
        let auctions = schema.auctions();
        let auctions = auctions.iter().map(|(_k, v)| v).collect::<Vec<_>>();
        Ok(auctions)
    }

    /// Wires the above endpoint to public scope of the given `ServiceApiBuilder`.
    pub fn wire(builder: &mut ServiceApiBuilder) {
        builder
            .public_scope()
            .endpoint("v1/users", Self::get_users)
            .endpoint("v1/user", Self::get_user)
            .endpoint("v1/user/lots", Self::get_user_lots)
            .endpoint("v1/user/auctions", Self::get_users_auctions)
            .endpoint("v1/auctions", Self::get_auctions)
            .endpoint("v1/auction", Self::get_auction_with_bids)
            .endpoint("v1/auction/bids", Self::get_auction_bids)
            .endpoint("v1/lots", Self::get_lots)
            .endpoint("v1/lot", Self::get_lot);
    }
}
