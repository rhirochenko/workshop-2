//! Auctions structs.

use chrono::{DateTime, Utc};
use exonum::crypto::{Hash, PublicKey};

use super::proto;

/// User structure.
#[derive(Clone, Debug, ProtobufConvert)]
#[exonum(pb = "proto::User", serde_pb_convert)]
pub struct User {
    /// Public key.
    pub user_id: PublicKey,
    /// User name.
    pub name: String,
    /// Current balance.
    pub balance: u64,
    /// Reserved money for the auction.
    pub reserved: u64,
}

impl User {
    /// Create User
    pub fn new(user_id: &PublicKey, name: &str, balance: u64, reserved: u64) -> Self {
        Self {
            user_id: user_id.to_owned(),
            name: name.to_owned(),
            balance,
            reserved,
        }
    }
}

/// Auction Lot base info stored in the database.
#[derive(Clone, Debug, ProtobufConvert)]
#[exonum(pb = "proto::LotInfo", serde_pb_convert)]
pub struct LotInfo {
    /// Lot item title.
    pub title: String,
    /// Lot description
    pub desc: String,
}

impl LotInfo {
    /// Create lot info
    pub fn new(title: &str, desc: &str) -> Self {
        Self {
            title: title.to_owned(),
            desc: desc.to_owned(),
        }
    }
}

/// Auction Lot info stored in the database.
#[derive(Clone, Debug, ProtobufConvert)]
#[exonum(pb = "proto::Lot", serde_pb_convert)]
pub struct Lot {
    /// Lot identifier.
    pub id: u64,
    /// Lot.
    pub lot_info: LotInfo,
    /// Current lot owner
    pub owner: PublicKey,
}

impl Lot {
    pub fn new(id: u64, lot_info: LotInfo, owner: &PublicKey) -> Self {
        Self {
            id,
            lot_info,
            owner: owner.to_owned(),
        }
    }
}

/// Auction base info.
#[derive(Clone, Debug, ProtobufConvert)]
#[exonum(pb = "proto::AuctionInfo", serde_pb_convert)]
pub struct AuctionInfo {
    /// Auctioneer identifier.
    pub auctioneer: PublicKey,
    /// Auction lot identifier.
    pub lot_id: u64,
    /// Start price.
    pub start_price: u64,
}

impl AuctionInfo {
    pub fn new(&auctioneer: &PublicKey, lot_id: u64, start_price: u64) -> Self {
        Self {
            auctioneer,
            lot_id,
            start_price,
        }
    }
}

/// Auction info
#[derive(Clone, Debug, ProtobufConvert)]
#[exonum(pb = "proto::Auction", serde_pb_convert)]
pub struct Auction {
    /// Auction identifier.
    pub auction_id: u64,
    /// Auction information.
    pub auction_info: AuctionInfo,
    /// Start time of the auction.
    pub started_at: DateTime<Utc>,
    /// Merkle root of history of bids. Last bid wins.
    pub bidding_merkle_root: Hash,
    /// If closed then no more bids are accepted.
    pub closed: bool,
}

impl Auction {
    pub fn new(
        auction_id: u64,
        auction_info: AuctionInfo,
        started_at: DateTime<Utc>,
        bidding_merkle_root: &Hash,
        closed: bool,
    ) -> Self {
        Self {
            auction_id,
            auction_info,
            started_at,
            bidding_merkle_root: bidding_merkle_root.to_owned(),
            closed,
        }
    }
}

/// Auction bid.
#[derive(Clone, Debug, ProtobufConvert)]
#[exonum(pb = "proto::Bid", serde_pb_convert)]
pub struct Bid {
    /// Bidder.
    pub bidder: PublicKey,
    /// Bid value.
    pub value: u64,
}

impl Bid {
    pub fn new(bidder: &PublicKey, value: u64) -> Self {
        Self {
            bidder: bidder.to_owned(),
            value,
        }
    }
}
