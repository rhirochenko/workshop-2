#[macro_export]
macro_rules! derive_sign {
    (
        $( #[$attr:meta] )*
        $(pub)* struct $name:ident
        {
            $(
                $( #[$field_attr:meta] )*
                   $(pub)+ $i:ident : $t:ty
            ),*
                $(,)*
        }
    ) => {
        $( #[$attr] )*
        pub struct $name {
            $( $( #[$field_attr] )* pub $i : $t, )*
        }

        impl $name {
            pub fn sign(
                author: &exonum::crypto::PublicKey,
                $( $i : $t, )*
                key: &exonum::crypto::SecretKey
            ) -> exonum::messages::Signed<exonum::messages::RawTransaction> {
                exonum::messages::Message::sign_transaction(
                    Self {
                        $($i),*
                    },
                    SERVICE_ID,
                    *author,
                    key
                )
            }
        }
    }
}
