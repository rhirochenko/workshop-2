//! Exonum Auctino transactions.

// Workaround for `failure` see https://github.com/rust-lang-nursery/failure/issues/223 and
// ECR-1771 for the details.
#![allow(bare_trait_objects)]

use exonum::{
    blockchain::{ExecutionError, ExecutionResult, Transaction, TransactionContext},
    crypto::{CryptoHash, Hash},
};
use exonum_time::schema::TimeSchema;

use crate::{auction::*, dto::*, schema::Schema, ISSUE_AMOUNT};

/// Error codes emitted by wallet transactions during execution.
#[derive(Debug, Fail)]
#[repr(u8)]
pub enum Error {
    /// Custom error code.
    ///
    /// Can be emitted by `Transaction name`.
    #[fail(display = "User is already registered")]
    UserAlreadyRegistered = 1,

    #[fail(display = "User is not registered")]
    UserIsNotRegistered = 2,

    #[fail(display = "Insufficient funds.")]
    InsufficientFunds = 3,

    #[fail(display = "Not your property.")]
    AccessViolation = 4,

    #[fail(display = "Lot does not exist")]
    LotNotFound = 5,

    #[fail(display = "You do not own of the item")]
    LotNotOwned = 6,

    #[fail(display = "Lot is already auctioned")]
    LotAlreadyAuctioned = 7,

    #[fail(display = "Auction does not exist")]
    AuctionNotFound = 8,

    #[fail(display = "Auction is closed")]
    AuctionClosed = 9,

    #[fail(display = "Bid is below the current highest bid")]
    BidTooLow = 10,

    // CloseAuction can only be performed by the validator nodes.
    #[fail(display = "Transaction is not authorized.")]
    UnauthorizedTransaction = 11,

    #[fail(display = "You may not bid on your own item.")]
    NoSelfBidding = 12,
}

impl From<Error> for ExecutionError {
    fn from(value: Error) -> ExecutionError {
        let description = format!("{}", value);
        ExecutionError::with_description(value as u8, description)
    }
}

/// Transaction group.
#[derive(Serialize, Deserialize, Clone, Debug, TransactionSet)]
pub enum AuctionTransactions {
    /// Transaction to create a new user.
    CreateUser(TxCreateUser),
    /// Transaction to issue funds.
    Issue(TxIssue),
    /// Transaction to create new auction.
    CreateAuction(TxCreateAuction),
    /// Transaction to make a lot
    MakeLot(TxMakeLot),
    /// Transaction to make a bid
    MakeBid(TxMakeBid),
}

impl Transaction for TxCreateUser {
    fn execute(&self, mut context: TransactionContext) -> ExecutionResult {
        let user_key = &context.author();
        let mut schema = Schema::new(context.fork());

        // Reject tx if the user with the same public key is already exists.
        if schema.users().get(user_key).is_some() {
            Err(Error::UserAlreadyRegistered)?;
        }

        let user = User::new(user_key, &self.name, ISSUE_AMOUNT, 0);
        schema.users_mut().put(user_key, user);

        Ok(())
    }
}

impl Transaction for TxIssue {
    fn execute(&self, mut context: TransactionContext) -> ExecutionResult {
        let key = &context.author();

        let mut schema = Schema::new(context.fork());
        let user = schema.users().get(key).unwrap();

        schema.increase_user_balance(&user.user_id, ISSUE_AMOUNT);
        Ok(())
    }
}

impl Transaction for TxCreateAuction {
    fn execute(&self, mut context: TransactionContext) -> ExecutionResult {
        let ts = TimeSchema::new(context.fork())
            .time()
            .get()
            .expect("Can't get the time");

        let mut schema = Schema::new(context.fork());
        let auction_info = AuctionInfo::new(&self.user_id, self.lot_id, self.start_price);

        // Check if the user is registered.
        let user = schema
            .users()
            .get(&auction_info.auctioneer)
            .ok_or_else(|| Error::UserIsNotRegistered)?;

        // Check if the lot exists.
        let lot = schema
            .lots()
            .get(&auction_info.lot_id.hash())
            .ok_or_else(|| Error::LotNotFound)?;

        // Check if the user owns the lot.
        if lot.owner != user.user_id {
            Err(Error::LotNotOwned)?;
        }

        // Check if the lot isn't auctioned already.
        if schema.lot_auction().get(&auction_info.lot_id).is_some() {
            Err(Error::LotAlreadyAuctioned)?;
        }

        // Establish a new auction.
        let auction_id = self.auction_id;
        let lot_id = auction_info.lot_id;
        let auction = Auction::new(auction_id, auction_info, ts, &Hash::zero(), false);

        schema.auctions_mut().put(&auction_id.hash(), auction);
        schema.lot_auction_mut().put(&lot_id, auction_id);
        schema.user_auctions_mut(&user.user_id).push(auction_id);

        Ok(())
    }
}

impl Transaction for TxMakeLot {
    fn execute(&self, mut context: TransactionContext) -> ExecutionResult {
        let mut schema = Schema::new(context.fork());
        let lot_info = LotInfo::new(&self.title, &self.desc);

        // Check if the user is registered.
        let user = schema
            .users()
            .get(&self.user_id)
            .ok_or_else(|| Error::UserIsNotRegistered)?;

        // Add a new lot.
        let lot = Lot::new(self.lot_id, lot_info, &self.user_id);

        schema.lots_mut().put(&self.lot_id.hash(), lot);
        schema.user_lots_mut(&user.user_id).push(self.lot_id);

        Ok(())
    }
}

impl Transaction for TxMakeBid {
    fn execute(&self, mut context: TransactionContext) -> ExecutionResult {
        let mut schema = Schema::new(context.fork());

        // Check if the user is registered.
        let user = schema
            .users()
            .get(&self.user_id)
            .ok_or_else(|| Error::UserIsNotRegistered)?;

        // Check if the auction exists.
        let auction = schema
            .auctions()
            .get(&self.auction_id.hash())
            .ok_or_else(|| Error::AuctionNotFound)?;

        let auction_info = auction.auction_info;

        // Check if the auction is open.
        if auction.closed {
            Err(Error::AuctionClosed)?;
        }

        // Check if the user has enough funds.
        if user.balance < self.value {
            Err(Error::InsufficientFunds)?;
        }

        // Bidding in own auction is prohibited.
        if user.user_id == auction_info.auctioneer {
            Err(Error::NoSelfBidding)?;
        }

        // Get the bid to beat.
        let min_bid = match schema.bids(auction.auction_id).last() {
            Some(bid) => bid.value,
            None => auction_info.start_price,
        };

        // Check if the bid is higher than the min bid.
        if min_bid >= self.value {
            Err(Error::BidTooLow)?;
        }

        // Release balance of the previous bidder if any.
        if let Some(b) = schema.bids(auction.auction_id).last() {
            let prev_bid_user = schema.users().get(&b.bidder).unwrap();
            schema.release_user_balance(&prev_bid_user.user_id, min_bid);
        }

        // Reserve value in user wallet.
        schema.reserve_user_balance(&user.user_id, self.value);

        // Make a bid.
        let bid = Bid::new(&self.user_id, self.value);
        schema.bids_mut(self.auction_id).push(bid);

        // Refresh the auction state.
        let bids_merkle_root = schema.bids(self.auction_id).merkle_root();
        schema.auctions_mut().put(
            &auction.auction_id.hash(),
            Auction::new(
                auction.auction_id,
                auction_info,
                auction.started_at,
                &bids_merkle_root,
                auction.closed,
            ),
        );

        Ok(())
    }
}
