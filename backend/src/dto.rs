use super::proto;
use crate::SERVICE_ID;
use chrono::{DateTime, Utc};
use exonum::crypto::PublicKey;

derive_sign! {
    /// Transaction to create a new user.
    #[derive(Serialize, Deserialize, Clone, Debug, ProtobufConvert)]
    #[exonum(pb = "proto::TxCreateUser")]
    pub struct TxCreateUser {
        /// Public key, used as identifier.
        pub user_id: PublicKey,
        /// Name.
        pub name: String,
    }
}

derive_sign! {
    /// Transaction to issue funds.
    #[derive(Serialize, Deserialize, Clone, Debug, ProtobufConvert)]
    #[exonum(pb = "proto::TxIssue")]
    pub struct TxIssue {
        /// Public user identifier.
        pub user_id: PublicKey,
        /// Seed to avoid idempotence.
        pub seed: DateTime<Utc>,
    }
}

derive_sign! {
    /// Transaction to create new auction.
    #[derive(Serialize, Deserialize, Clone, Debug, ProtobufConvert)]
    #[exonum(pb = "proto::TxCreateAuction")]
    pub struct TxCreateAuction {
        /// Auctioneer.
        pub user_id: PublicKey,
        /// Auction id
        pub auction_id: u64,
        /// Lot with `lot_id` is auctioned.
        pub lot_id: u64,
        /// Start price.
        pub start_price: u64,
        /// Seed to avoid idempotence.
        pub seed: u64,
    }
}

derive_sign! {
    /// Transaction to create an auction lot.
    #[derive(Serialize, Deserialize, Clone, Debug, ProtobufConvert)]
    #[exonum(pb = "proto::TxMakeLot")]
    pub struct TxMakeLot {
        /// Lot creator.
        pub user_id: PublicKey,
        /// Lot id
        pub lot_id: u64,
        /// Lot title.
        pub title: String,
        /// Lot description.
        pub desc: String,
        /// Seed to avoid idempotence.
        pub seed: u64,
    }
}

derive_sign! {
    /// Transaction to make a bid
    #[derive(Serialize, Deserialize, Clone, Debug, ProtobufConvert)]
    #[exonum(pb = "proto::TxMakeBid")]
    pub struct TxMakeBid {
        /// Bidder.
        pub user_id: PublicKey,
        /// Auction ID where a bid must be made.
        pub auction_id: u64,
        /// Bid value.
        pub value: u64,
    }
}
