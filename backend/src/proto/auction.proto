syntax = "proto3";

package auction;

import "helpers.proto";
import "google/protobuf/timestamp.proto";

/// Transaction to create a new user.
message TxCreateUser {
    /// Public key, used as identifier.
    exonum.PublicKey user_id = 1;
    /// Name.
    string name = 2;
}

/// User structure.
message User {
    /// Public key.
    exonum.PublicKey user_id = 1;
    /// User name.
    string name = 2;
    /// Current balance.
    uint64 balance = 3;
    /// Reserved money for the auction.
    uint64 reserved = 4;
}

/// Transaction to issue funds.
message TxIssue {
    /// Public user identifier.
    exonum.PublicKey user_id = 1;
    /// Seed to avoid idempotence.
    google.protobuf.Timestamp seed = 2;
}

/// Transaction to create new auction.
message TxCreateAuction {
    /// Auctioneer.
    exonum.PublicKey user_id = 1;
    /// Auction id
    uint64 auction_id = 2;
    /// Lot with `lot_id` is auctioned.
    uint64 lot_id = 3;
    /// Start price.
    uint64 start_price = 4;
    /// Seed to avoid idempotence.
    uint64 seed = 5;
}

/// Transaction to create an auction lot.
message TxMakeLot {
    /// Lot creator.
    exonum.PublicKey user_id = 1;
    /// Lot id
    uint64 lot_id = 2;
    /// Lot title.
    string title = 3;
    /// Lot description.
    string desc = 4;
    /// Seed to avoid idempotence.
    uint64 seed = 5;
}

/// Transaction to make a bid
message TxMakeBid {
    /// Bidder.
    exonum.PublicKey user_id = 1;
    /// Auction ID where a bid must be made.
    uint64 auction_id = 2;
    /// Bid value.
    uint64 value = 3;
}

/// Auction Lot base info.
message LotInfo {
    /// Lot item title.
    string title = 1;
    /// Lot description
    string desc = 2;
}

/// Auction Lot.
message Lot {
    /// Lot identifier.
    uint64 id = 1;
    /// Lot.
    LotInfo lot_info = 2;
    /// Current lot owner
    exonum.PublicKey owner = 3;
}

/// Auction base info.
message AuctionInfo {
    /// Auctioneer identifier.
    exonum.PublicKey auctioneer = 1;
    /// Auction lot identifier.
    uint64 lot_id = 2;
    /// Start price.
    uint64 start_price = 3;
}

/// Auction info
message Auction {
    /// Auction identifier.
    uint64 auction_id = 1;
    /// Auction information.
    AuctionInfo auction_info = 2;
    /// Start time of the auction.
    google.protobuf.Timestamp started_at = 3;
    /// Merkle root of history of bids. Last bid wins.
    exonum.Hash bidding_merkle_root = 4;
    /// If closed then no more bids are accepted.
    bool closed = 5;
}

/// Auction bid.
message Bid {
    /// Bidder.
    exonum.PublicKey bidder = 1;
    /// Bid value.
    uint64 value = 2;
}
