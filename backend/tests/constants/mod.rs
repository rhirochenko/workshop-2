//! Shared constants used both in api and transactions logic tests.

/// Alice's wallets name.
pub const ALICE_NAME: &str = "Alice";
/// Bob's wallet name.
pub const BOB_NAME: &str = "Bob";

