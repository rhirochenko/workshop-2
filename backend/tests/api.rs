#[macro_use]
extern crate serde_json;

use exonum::{
    api::node::public::explorer::{TransactionQuery, TransactionResponse},
    crypto::{self, Hash, PublicKey, SecretKey},
    helpers::Height,
    messages::{self, RawTransaction, Signed},
};
use exonum_testkit::{ApiKind, TestKit, TestKitApi, TestKitBuilder};
use exonum_time::{time_provider::MockTimeProvider, TimeService};
use std::time::SystemTime;

// Import data types used in tests from the crate where the service is defined.
use exonum_auction::{api::*, auction::*, dto::*, Service};

// Imports shared test constants.
use crate::constants::{ALICE_NAME, BOB_NAME};

mod constants;

/// Check that the user creation transaction works when invoked via API.
#[test]
fn test_create_user() {
    let (mut testkit, api) = create_testkit();
    // Create and send a transaction via API
    let (tx, _, _) = api.create_user(ALICE_NAME);
    testkit.create_block();
    api.assert_tx_status(tx.hash(), &json!({ "type": "success" }));

    // Check that the user indeed is persisted by the service.
    let user = api.get_user(tx.author());
    assert_eq!(user.user_id, tx.author());
    assert_eq!(user.name, ALICE_NAME);
    assert_eq!(user.balance, 100);
}

/// Check create lot.
#[test]
fn test_create_lot() {
    // Create 2 users.
    let (mut testkit, api) = create_testkit();
    let (tx_alice, pub_key_alice, sec_key_alice) = api.create_user(ALICE_NAME);
    let (tx_bob, pub_key_bob, sec_key_bob) = api.create_user(BOB_NAME);
    testkit.create_block();
    api.assert_tx_status(tx_alice.hash(), &json!({ "type": "success" }));
    api.assert_tx_status(tx_bob.hash(), &json!({ "type": "success" }));

    // Create lot
    let lot_id = 1;
    let lot_title = "First_lot";
    let (tx_create_lot, _) = api.create_lot(pub_key_alice, sec_key_alice, lot_id, lot_title);
    testkit.create_block();
    api.assert_tx_status(tx_create_lot.hash(), &json!({ "type": "success" }));

    /// Get lot
    let lot = api.get_lot(lot_id).unwrap();
    assert_eq!(lot.id, lot_id);
    assert_eq!(lot.lot_info.title, lot_title)
}

/// Check create auction and make bid
// TODO: ***Code here***

/// Creates a testkit together with the API wrapper defined above.
fn create_testkit() -> (TestKit, AuctionApi) {
    let mock_provider = MockTimeProvider::new(SystemTime::now().into());
    let mut testkit = TestKitBuilder::validator()
        .with_service(Service)
        .with_service(TimeService::with_provider(mock_provider.clone()))
        .create();
    testkit.create_blocks_until(Height(2));
    let api = AuctionApi {
        inner: testkit.api(),
    };
    (testkit, api)
}

/// Wrapper for the cryptocurrency service API allowing to easily use it
/// (compared to `TestKitApi` calls).
struct AuctionApi {
    pub inner: TestKitApi,
}

impl AuctionApi {
    /// Generates a user creation transaction with a random key pair
    fn create_user(&self, name: &str) -> (Signed<RawTransaction>, PublicKey, SecretKey) {
        let (pub_key, sec_key) = crypto::gen_keypair();
        // Create a pre-signed transaction
        let tx = TxCreateUser::sign(&pub_key, pub_key, name.to_string(), &sec_key);

        let data = messages::to_hex_string(&tx);
        let tx_info: TransactionResponse = self
            .inner
            .public(ApiKind::Explorer)
            .query(&json!({ "tx_body": data }))
            .post("v1/transactions")
            .unwrap();
        assert_eq!(tx_info.tx_hash, tx.hash());
        (tx, pub_key, sec_key)
    }

    /// Get user info
    fn get_user(&self, user_id: PublicKey) -> User {
        let user_info = self
            .inner
            .public(ApiKind::Service("auction"))
            .query(&UserQuery { user_id })
            .get::<User>("v1/user")
            .unwrap();
        user_info
    }

    /// Generates a auction creation transaction with a random key pair
    fn create_auction(
        &self,
        pub_key: PublicKey,
        sec_key: SecretKey,
        lot_id: u64,
        auction_id: u64,
    ) -> (Signed<RawTransaction>, SecretKey) {
        // Create a pre-signed transaction
        let tx = TxCreateAuction::sign(&pub_key, pub_key, auction_id, lot_id, 0, 0, &sec_key);
        let data = messages::to_hex_string(&tx);
        let tx_info: TransactionResponse = self
            .inner
            .public(ApiKind::Explorer)
            .query(&json!({ "tx_body": data }))
            .post("v1/transactions")
            .unwrap();
        assert_eq!(tx_info.tx_hash, tx.hash());
        (tx, sec_key)
    }

    /// Get auction bids info
    fn get_auction_with_bids(&self, auction_id: u64) -> Option<(Auction, Vec<Bid>)> {
        let auction_with_bids = self
            .inner
            .public(ApiKind::Service("auction"))
            .query(&AuctionQuery { auction_id })
            .get::<Option<(Auction, Vec<Bid>)>>("v1/auction")
            .unwrap();
        auction_with_bids
    }

    /// Generates a lot transaction
    fn create_lot(
        &self,
        pub_key: PublicKey,
        sec_key: SecretKey,
        lot_id: u64,
        title: &str,
    ) -> (Signed<RawTransaction>, SecretKey) {
        let tx = TxMakeLot::sign(
            &pub_key,
            pub_key,
            lot_id,
            title.to_string(),
            "".to_string(),
            0,
            &sec_key,
        );
        let data = messages::to_hex_string(&tx);
        let tx_info: TransactionResponse = self
            .inner
            .public(ApiKind::Explorer)
            .query(&json!({ "tx_body": data }))
            .post("v1/transactions")
            .unwrap();
        assert_eq!(tx_info.tx_hash, tx.hash());
        (tx, sec_key)
    }

    /// Get lot info
    fn get_lot(&self, lot_id: u64) -> Option<Lot> {
        let lot_info = self
            .inner
            .public(ApiKind::Service("auction"))
            .query(&LotQuery { lot_id })
            .get::<Option<Lot>>("v1/lot")
            .unwrap();
        lot_info
    }

    /// Generates a bid
    fn make_bid(
        &self,
        pub_key: PublicKey,
        sec_key: SecretKey,
        auction_id: u64,
        bid: u64,
    ) -> (Signed<RawTransaction>, SecretKey) {
        let tx = TxMakeBidt::sign(&pub_key, pub_key, auction_id, bid, &sec_key);
        let data = messages::to_hex_string(&tx);
        let tx_info: TransactionResponse = self
            .inner
            .public(ApiKind::Explorer)
            .query(&json!({ "tx_body": data }))
            .post("v1/transactions")
            .unwrap();
        assert_eq!(tx_info.tx_hash, tx.hash());
        (tx, sec_key)
    }

    /// Asserts that the transaction with the given hash has a specified status.
    fn assert_tx_status(&self, tx_hash: Hash, expected_status: &serde_json::Value) {
        let info: serde_json::Value = self
            .inner
            .public(ApiKind::Explorer)
            .query(&TransactionQuery::new(tx_hash))
            .get("v1/transactions")
            .unwrap();

        if let serde_json::Value::Object(mut info) = info {
            let tx_status = info.remove("status").unwrap();
            assert_eq!(tx_status, *expected_status);
        } else {
            panic!("Invalid transaction info format, object expected");
        }
    }
}
