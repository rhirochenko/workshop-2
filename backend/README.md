# Exonum Workshop #2: Auction transaction implementation

## Plan:
1. Switch to 'workshop' branch
2. Implement TxCreateAuction transaction business logic
3. Implement auction API
4. Add missed code to TxMakeBid execute function
5. Add test to check auction API and transactions
6. Build application and test with external POST requests